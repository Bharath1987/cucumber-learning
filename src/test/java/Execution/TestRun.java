package Execution;

import cucumber.api.CucumberOptions;
//import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/java/DeclarativeApproach/TestLeafLogin.feature", glue="Steps", monochrome=true /*dryRun=true, snippets=SnippetType.CAMELCASE*/)
public class TestRun extends AbstractTestNGCucumberTests {


}
