package Steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LeadSteps {
	public ChromeDriver driver;
	@Given("Open the Chrome Browser")
	public void openTheChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set the timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@And("Load the url")
	public void loadTheUrl() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("Enter the Username as (.*)")
	public void enterTheUsername(String UName) {
		driver.findElementById("username").sendKeys(UName);
	}

	@And("Enter the password as (.*)")
	public void enterThePassword(String Password) {
		driver.findElementById("password").sendKeys(Password);
	
	}

	@When("Click on login button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
		
	}

	@Then("User should navigate the Homepage")
	public void userShouldNavigateTheHomepage() {
	    System.out.println("Homepage navigated");
	}



}


